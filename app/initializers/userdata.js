export default {
  name: 'userdataInit',
  after: 'structure',
  initialize: function(container, app) {
    app.deferReadiness();

    var store = container.lookup('store:main');

    if(store.find('userdata').length < 100){
      
      store.findAll('day').then( function(day) {
        var today = day;
        day.workout.forEach(function(workout){
          for(i = 1; i <= workout.sets; i++){
            var setItem = {
              set: i,
              weight: '',
              reps: workout.reps,
              day: today.id
            };
            var record = store.createRecord(setItem);
            //record.save();
         }
        }); 
      });

    }

    
    app.advanceReadiness();
  }
};
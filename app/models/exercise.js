import DS from 'ember-data';

var Exercise = DS.Model.extend({
  name: DS.attr('string'),
  description: DS.attr('string'),
  icon: DS.attr('string'),
  video: DS.attr('string'),
  cadence: DS.attr('string'),
  showWeight: DS.attr('boolean'),
  workout: DS.hasMany('workout', {async: true})
});

export default Exercise;
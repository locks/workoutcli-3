export default function(){

  this.transition(
    this.toRoute('exercises'),
    this.use('toRight'),
    this.reverse('toLeft')
  );

  this.transition(
    this.toRoute('settings'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('weeks'),
    this.toRoute('week'),
    this.use('toUp'),
    this.reverse('toDown')
    //this.use('explode', {
    //  matchBy: 'data-week',
    //  use: ['flyTo']
    //})
    //  this.use('explode', {
    //    matchBy: 'data-week',
    //    use: ['flyTo']
    //  })
  );



}